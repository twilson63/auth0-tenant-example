const express = require('express')
const core = require('core')
const { path } = require('ramda')
const checkJwt = require('./check-jwt')

const app = express()
module.exports = app

app.use(checkJwt)

/**
 * get pet
 */
app.get('/pets/:id', async (req, res) => {
  const store = path(['user', 'https://pets.dev/tenant'], req)
  const id = path(['params', 'id'], req)
  if (!store) return res.status(500).send({ message: 'pet store is required'})
  const pet = await core.pets.get(id, store)
  return res.send(pet)
})

/**
 * get all pets of a store
 */
app.get('/pets', async (req, res) => {
  const store = path(['user', 'https://pets.dev/tenant'], req)
  if (!store) return res.status(500).send({ message: 'pet store is required' })
  return res.send(await core.pets.all(store))
})

/**
 * add new pet to a store
 */
app.post('/pets', express.json(), async (req, res) => {
  const store = path(['user', 'https://pets.dev/tenant'], req)
  if (!store) return res.status(500).send({ message: 'pet store is require'})
  req.body.store = store
  return res.send(await core.pets.create(req.body))
})



