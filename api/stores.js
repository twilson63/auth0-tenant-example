const express = require('express')
const core = require('core')
const { path } = require('ramda')

const app = express()

app.post('/stores', express.json(), async (req, res) => 
  res.send(
    await core.stores.create(
      path(['body', 'name'], req)
    )
  )
)

app.get('/stores', async (req, res) => res.send(await core.stores.all()))


module.exports = app
