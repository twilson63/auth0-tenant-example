const express = require('express')
const core = require('core')
const checkJwt = require('./check-jwt')
const { path } = require('ramda')

const app = express()

app.use(checkJwt)

app.get('/users', async (req, res) => {
  const store = path(['user', 'https://pets.dev/tenant'], req)
  if (!store) { return res.status(500).send({ message: 'store is required'}) }
  return res.send(await core.users.all(store))
})

app.post('/users', express.json(), async (req, res) => {
  const store = path(['user', 'https://pets.dev/tenant'], req)
  if (!store) { return res.status(500).send({ message: 'store is required'}) }
  req.body.store = store
  try {
    res.send(await core.users.create(req.body))
  } catch (err) {
    console.log(err)
    res.status(500).send({message: err.message})
  }
})

module.exports = app
