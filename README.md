# Auth0 multi-tenant example

[![pipeline status](https://gitlab.com/twilson63/auth0-tenant-example/badges/master/pipeline.svg)](https://gitlab.com/twilson63/auth0-tenant-example/-/commits/master)

This is a sample project that demonstrates one way to create a multi-tenant login system using auth0. This does not deal with the architecture design of a multi-tenant system. It simply demonstrates how you might manage/support users in a multi-tenant environment using auth0. This example is assuming that multi-tenant architecture is an invite-only system, where the users have to be invited to join for a particular tenant.

The example application, will be a pet store content management system, each tenant will be an individual pet store that will contain an inventory of pets that they might have for sell. There will be two roles in the system, basic and admin. 

## Developer Setup

```
yarn 
yarn build
yarn start
```

## Features

* login/logout
* list pet inventory for store
* create a new pet
* edit a pet
* remove a pet
* view a pet
* list users
* add a user
* remove a user

## Roles

* basic
  - login/logout
  - list pets
  - create pet
  - view pet
  - edit pet
  - remove pet

* admin
  - all basic features
  - list users
  - add user
  - remove user

## Tasks

* create repo
* setup express project
* setup core lib
* setup svelte app
* setup tests
* setup ci/cd

## Stories


### Business Rules

* pet object
* user object
* store object

### Pages

* list pets
* add pet
* view pet
* edit pet
* remove pet

* list users
* add user
* remove user


