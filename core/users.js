const { curry } = require('ramda')

module.exports = {
  name: 'users',
  all: curry((store, { details }) => {
    const authSvc = details.auth0Service

    return authSvc.listUsers(store) 
  }),
  create: curry((user, { details }) => {
    const authSvc = details.auth0Service
    return authSvc.createUser(user) 
  })
}
