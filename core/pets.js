const { curry } = require('ramda')

module.exports = {
  name: 'pets',
  all: curry((store, {details}) => {
    const dal = details.dal
    return dal.allPets(store)
  }),
  create: curry((pet, {details}) => {
    const dal = details.dal
    return dal.createPet(pet)
  }),
  get: curry((id, store, { details}) => {
    const dal = details.dal
    return dal.getPet(id, store)
  }),
  update: curry((pet, store, { details }) => {
    const dal = details.dal
    return dal.updatePet(pet, store)
  }),
  remove: curry((pet, store, { details }) => {
    const dal = details.dal
    return dal.removePet(pet, store)
  })
}
