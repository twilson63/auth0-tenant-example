const createBundle = require('@twilson63/bob')

const stores = require('./stores')
const pets = require('./pets')
const users = require('./users')

const dal = require('./dal')
const auth0Service = require('./auth0-service')

module.exports = createBundle([
  stores,
  pets,
  users
], { dal, auth0Service })


