const { compose, not, map, and, find, append, filter, propEq } = require('ramda')
const cuid = require('cuid')
const { uniqueNamesGenerator, adjectives, colors, animals } = require("unique-names-generator")

const createStoreName = () => uniqueNamesGenerator({
  dictionaries: [adjectives, colors, animals]
})
const DEBUG = process.env.NODE_ENV === 'DEBUG'

let stores = ['store1', 'store2']
let pets = [{
  id: "1",
  name: 'ribbons',
  type: 'Dog',
  store: 'store1'
}]

module.exports = {
  createStore(name) {
    if (!name) {
      name = createStoreName()
    }
    stores = append(name, stores)  
    return Promise.resolve({ ok: true, name })
  },
  allStores() {
    if (DEBUG) console.log('STORES: ' + JSON.stringify(stores))
    return Promise.resolve(stores)
  },
  allPets(store) {
    if (!store) return Promise.reject({message: 'STORE is required!'})
    return Promise.resolve(filter(propEq('store',store), pets))
  },
  createPet(pet) {
    pet.id = cuid()
    pets = append(pet, pets)
    return Promise.resolve({ok: true, pet})
  },
  getPet(id, store) {
    const pet = find(and(propEq('store', store), propEq('id', id)), pets)
    return Promise.resolve(pet)
  },
  
  updatePet(pet, store) {
    pets = map(p => {
      if (pet.id === p.id) {
        return pet
      }
      return p
    }, pets)

    return Promise.resolve({ok: true, pet})
  },
  
  removePet(pet, store) {
    pets = filter(p => compose(not, propEq('id', p.id)), pets)
    return Promise.resolve({ok: true, pet})
  }
}
