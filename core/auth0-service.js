const auth0Sdk = require('auth0')
const ManagementClient = auth0Sdk.ManagementClient
const { filter, pathEq } = require('ramda')

if (!process.env.AUTH0_DOMAIN) { throw new Error('AUTH0 Credentials Not found in environment!') }

const auth0 = new ManagementClient({
  domain: process.env.AUTH0_DOMAIN,
  clientId: process.env.AUTH0_CLIENT_ID,
  clientSecret: process.env.AUTH0_CLIENT_SECRET,
  scope: 'read:users create:users update:users'
})

module.exports = {
  //** add methods here
  async listUsers(store) {
    // get users from auth0
    const users = await auth0.getUsers()
    // filter users by store which equals app_metadata.tenant
    const usersByStore = filter(pathEq(['app_metadata', 'tenant'], store), users)
    // return filtered list
    return usersByStore
  },
  async createUser(user) {
    // assign roles to user
    let roles = ['rol_RS913jWE6rjPXJKv']
    if (user.isAdmin) {
      roles = roles.concat(['rol_hA9ZQXzQF8j9br7d'])
    }
    // create user with email and password
    user = await auth0.createUser({
      email: user.email,
      password: user.password,
      app_metadata: {
        tenant: user.store
      },
      connection: 'Username-Password-Authentication'
    })
    await auth0.assignRolestoUser({ id: user.user_id }, { roles })
    return { ok: true, user }
  }
}
