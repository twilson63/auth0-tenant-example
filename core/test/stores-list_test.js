const test = require('tape')

const core = require('../')

test('list stores', async t => {
  try {
    const stores = await core.stores.all()
    t.ok(stores)
    t.ok(stores.length > 0)
    t.end()
  } catch (e) {
    console.log(e.message)
  }
})
