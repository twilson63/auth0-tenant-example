const test = require('tape')
const core = require('../')

test('list pets for store1', async t => {
  const pets = await core.pets.all('store1')
  t.ok(pets.length > 0)
  t.end()
})
