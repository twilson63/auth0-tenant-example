const test = require('tape')
const core = require('../')

test('create pet', async t => {
  const result = await core.pets.create({
    id: 2,
    name: 'gretal',
    store: 'store1'
  })
  t.ok(result.ok)
  t.end()
})
