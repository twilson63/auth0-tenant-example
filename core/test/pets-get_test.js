const test = require('tape')
const core = require('../')

test('get pet by id', async t => {
  const pet = await core.pets.get("1", 'store1')
  t.ok(pet)
  t.equal(pet.name, 'ribbons')
  t.end()
})

