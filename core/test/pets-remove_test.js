const test = require('tape')
const core = require('../')

const example = {
  "name": "fluffy",
  "type": "Cat",
  "store": "store1"
}

test('remove pet', async t => {
  await core.pets.create(example)
  const result = await core.pets.remove(example, "store1")
  t.ok(result.ok)
  t.end()
})

