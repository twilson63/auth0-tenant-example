const test = require('tape')
const core = require('../')

test('update pet', async t => {
  const pet = await core.pets.get("1", "store1")
  pet.name = "ribbons2"
  const result = await core.pets.update(pet, "store1")
  t.ok(result.ok)
  t.equal(result.pet.name, 'ribbons2')
  const pet2 = await core.pets.get("1", "store1")
  t.equal(result.pet.name, pet2.name)
  t.end()
})

