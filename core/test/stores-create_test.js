const test = require('tape')
const core = require('../')

test('create a new store', async t => {
  const result = await core.stores.create('foo')
  t.ok(result.ok)
  const stores = await core.stores.all()
  t.equal(stores.length, 3)
  t.end()

})
