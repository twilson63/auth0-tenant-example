const { curry } = require('ramda')

module.exports = {
  name: 'stores',
  create: curry((store, { details }) => {
    const dal = details.dal
    return dal.createStore(store) 
  }),
  all: curry(({details}) => {
    const dal = details.dal
    return dal.allStores()
  })

}

