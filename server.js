require('dotenv').config()

const express = require('express')
const path = require('path')
const stores = require('./api/stores')
const pets = require('./api/pets')
const users = require('./api/users')

const app = express()

app.use('/api', stores, pets, users)
app.use(express.static('public'))
app.get('*', (req, res) => {
  res.sendFile(path.resolve('./public/index.html'))
})

app.listen(3000)
console.log('listening on port 3000')

